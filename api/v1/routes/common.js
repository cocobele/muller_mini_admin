const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const express = require('express');
const router = express.Router();
const handler = require('../handlers/common');
const multer  = require('multer');
const mStorage = multer.memoryStorage();

let mUpload = multer({
    storage: mStorage,
    limits:{
        files: 1, //一次只允许上传一个文件
        fileSize: 15000*1024  //设置文件大小不能超过1MB
    }
});

router.get('/get_app_profile', function(req, res) {
    handler.getAppProfile(req, res)
});

router.get('/get_oss_auth', function(req, res) {
    handler.getOSSAuth(req, res)
});

router.post('/upload_image', mUpload.single('image'), function(req, res, err) {
    handler.uploadImage(req, res)
});

module.exports = router;

