const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/product');

router.post('/add_product', function(req, res) {
    handler.addProduct(req, res)
});

router.post('/remove_product', function(req, res) {
    handler.removeProduct(req, res)
});

router.post('/edit_product', function(req, res) {
    handler.editProduct(req, res)
});

router.get('/get_product', function(req, res) {
    handler.getProduct(req, res)
});

router.get('/get_product_detail', function(req, res) {
    handler.getProductDetail(req, res)
});

router.post('/edit_product_disp_state', function(req, res) {
    handler.editProductDispState(req, res)
});

module.exports = router;

