const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/artist');

router.post('/add_artist', function(req, res) {
    handler.addArtist(req, res)
});

router.post('/remove_artist', function(req, res) {
    handler.removeArtist(req, res)
});

router.post('/edit_artist', function(req, res) {
    handler.editArtist(req, res)
});

router.get('/get_artist', function(req, res) {
    handler.getArtist(req, res)
});

router.get('/get_artist_detail', function(req, res) {
    handler.getArtistDetail(req, res)
});

router.post('/edit_artist_disp_state', function(req, res) {
    handler.editArtistDispState(req, res)
});

module.exports = router;

