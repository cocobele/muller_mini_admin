const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/exhibition');

router.post('/add_exhibition', function(req, res) {
    handler.addExhibition(req, res)
});

router.post('/remove_exhibition', function(req, res) {
    handler.removeExhibition(req, res)
});

router.post('/edit_exhibition', function(req, res) {
    handler.editExhibition(req, res)
});

router.get('/get_exhibition', function(req, res) {
    handler.getExhibition(req, res)
});

router.get('/get_exhibition_detail', function(req, res) {
    handler.getExhibitionDetail(req, res)
});

router.post('/edit_exhibition_disp_state', function(req, res) {
    handler.editExhibitionDispState(req, res)
});

module.exports = router;

