const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/article');

router.post('/add_article', function(req, res) {
    handler.addArticle(req, res)
});

router.post('/remove_article', function(req, res) {
    handler.removeArticle(req, res)
});

router.post('/edit_article', function(req, res) {
    handler.editArticle(req, res)
});

router.get('/get_article', function(req, res) {
    handler.getArticle(req, res)
});

router.get('/get_article_detail', function(req, res) {
    handler.getArticleDetail(req, res)
});

router.post('/edit_article_disp_state', function(req, res) {
    handler.editArticleDispState(req, res)
});

module.exports = router;

