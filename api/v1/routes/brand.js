const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/brand');

router.post('/add_brand', function(req, res) {
    handler.addBrand(req, res)
});

router.post('/remove_brand', function(req, res) {
    handler.removeBrand(req, res)
});

router.post('/edit_brand', function(req, res) {
    handler.editBrand(req, res)
});

router.get('/get_brand', function(req, res) {
    handler.getBrand(req, res)
});

router.get('/get_brand_detail', function(req, res) {
    handler.getBrandDetail(req, res)
});

router.post('/edit_brand_disp_state', function(req, res) {
    handler.editBrandDispState(req, res)
});

module.exports = router;

