const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/user');

router.post('/add_user', function(req, res) {
    handler.addUser(req, res)
});

router.post('/remove_user', function(req, res) {
    handler.removeUser(req, res)
});

router.post('/edit_user', function(req, res) {
    handler.editUser(req, res)
});

router.get('/get_user', function(req, res) {
    handler.getUser(req, res)
});

router.get('/get_user_detail', function(req, res) {
    handler.getUserDetail(req, res)
});

module.exports = router;

