const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/mini_user');

router.get('/get_mini_user', function(req, res) {
    handler.getMiniUser(req, res)
});

module.exports = router;

