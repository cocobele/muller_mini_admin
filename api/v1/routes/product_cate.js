const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/product_cate');

router.post('/add_product_cate', function(req, res) {
    handler.addProductCate(req, res)
});

router.post('/remove_product_cate', function(req, res) {
    handler.removeProductCate(req, res)
});

router.post('/edit_product_cate', function(req, res) {
    handler.editProductCate(req, res)
});

router.get('/get_product_cate', function(req, res) {
    handler.getProductCate(req, res)
});

router.get('/get_product_cate_detail', function(req, res) {
    handler.getProductCateDetail(req, res)
});

router.post('/edit_product_cate_disp_state', function(req, res) {
    handler.editProductCateDispState(req, res)
});

module.exports = router;

