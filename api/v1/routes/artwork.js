const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/artwork');

router.post('/add_artwork', function(req, res) {
    handler.addArtwork(req, res)
});

router.post('/remove_artwork', function(req, res) {
    handler.removeArtwork(req, res)
});

router.post('/edit_artwork', function(req, res) {
    handler.editArtwork(req, res)
});

router.get('/get_artwork', function(req, res) {
    handler.getArtwork(req, res)
});

router.get('/get_artwork_detail', function(req, res) {
    handler.getArtworkDetail(req, res)
});

router.post('/edit_artwork_disp_state', function(req, res) {
    handler.editArtworkDispState(req, res)
});

module.exports = router;

