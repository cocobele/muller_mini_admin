const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/order');

router.get('/get_b2c_order', function(req, res) {
    handler.getB2COrder(req, res)
});

router.get('/get_b2c_order_detail', function(req, res) {
    handler.getB2COrderDetail(req, res)
});

router.post('/edit_b2c_order', function(req, res) {
    handler.editB2COrder(req, res)
});

router.post('/edit_b2c_order_trade_state', function(req, res) {
    handler.editB2COrderTradeState(req, res)
});

module.exports = router;

