const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/auth');

router.post('/login_pwd', function(req, res) {
    handler.loginPwd(req, res)
});

router.post('/logout', function(req, res) {
    handler.logout(req, res)
});

router.use(require(APP_BASE_PATH + 'middleware/auth').sessionValidate);

router.get('/get_session', function(req, res) {
    handler.getSession(req, res)
});

router.get('/get_current_user', function(req, res) {
    handler.getCurrentUser(req, res)
});

module.exports = router;

