const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports = {

    addArticle: async function(req, res){
        let param = req.body;

        const Article = getModel('article');
        const MergeArtistArticle = getModel('merge_artist_article');

        try {
            cu.checkFieldExists(param, 'title');
            cu.checkFieldExists(param, 'disp_state');
            cu.checkFieldExists(param, 'preview_pic_url');
            cu.checkFieldExists(param, 'abstract');
            cu.checkFieldExists(param, 'content');

            if(param['artist_id']){
                cu.checkFieldIsArray(param, 'artist_id');
            }
        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        param['id'] = uuidv1().replace(/-/g, '');

        param['modifier'] = ci.DEFAULT_MODIFIER;
        param['modify_time'] = Date.now();
        param['create_time'] = Date.now();

        let transaction = null;

        try {
            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let article = await Article.create(param, {transaction});
            param['id'] = article['id'];

            if(param['artist_id']) {
                await MergeArtistArticle.destroy({
                    where: {
                        article_id: param['id'],
                    },
                    transaction,
                });

                let artistArticle = [],
                    index = 0;
                for (let artistId of param['artist_id']) {
                    let relObj = {};
                    relObj['artist_id'] = artistId;
                    relObj['article_id'] = param['id'];
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArticle.push(relObj);
                }
                await MergeArtistArticle.bulkCreate(artistArticle, {transaction});
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: article['id'],
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editArticleDispState: async function(req, res){
        let param = req.body;

        const Article = getModel('article');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
            cu.checkFieldExists(param, 'disp_state');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let editParam = {};
        try {
            editParam['disp_state'] = param['disp_state'];
            editParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let article = await Article.update(editParam, {
                where: {
                    id: param['id'],
                },
                transaction,
            });

            if(article[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    removeArticle: async function(req, res){
        let param = req.body;

        const Article = getModel('article');
        const MergeArtistArticle = getModel('merge_artist_article');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let delParam = {};
        try {
            delParam['lstate'] = enums.DataLState.DELETED;
            delParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let article = await Article.update(delParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            await MergeArtistArticle.destroy({
                where: {
                    article_id: param['id'],
                },
                transaction,
            });

            if(article[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editArticle: async function(req, res){
        let param = req.body;

        const Article = getModel('article');
        const MergeArtistArticle = getModel('merge_artist_article');

        try {
            cu.checkFieldExists(param, 'id');

            if(param['artist_id']){
                cu.checkFieldIsArray(param, 'artist_id');
            }
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let article = await Article.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(article[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            if(param['artist_id']) {
                await MergeArtistArticle.destroy({
                    where: {
                        article_id: param['id'],
                    },
                    transaction,
                });

                let artistArticle = [],
                    index = 0;
                for (let artistId of param['artist_id']) {
                    let relObj = {};
                    relObj['artist_id'] = artistId;
                    relObj['article_id'] = param['id'];
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArticle.push(relObj);
                }
                await MergeArtistArticle.bulkCreate(artistArticle, {transaction});
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getArticle: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let tsNow = moment().unix();

        const Article = getModel('article');
        const Artist = getModel('artist');
        const MergeArtistArticle = getModel('merge_artist_article');

        let orderRule = [
            ['disp_state', 'ASC'],
            ['weight', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_article.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['query_title']){
            whereRule['$t_article.title$'] = {
                '$like': '%' + param['query_title'] + '%',
            }
        }

        if(param['disp_state']){
            whereRule['$t_article.disp_state$'] = {
                '$eq': param['disp_state'],
            }
        }

        let artistIds = [];
        if(param['query_artist_name']){
            artistIds = await Artist.findAll({
                attributes: [
                    'id',
                ],
                where: {
                    '$t_artist.name$': {
                        '$like': '%' + param['query_artist_name'] + '%',
                    }
                },
            }).map((obj)=>{
                return obj['id'];
            });
        }

        if(param['artist_id']){
            artistIds.push(param['artist_id']);
        }

        if(artistIds.length > 0){
            whereRule['id'] = [];
             await MergeArtistArticle.findAll({
                attributes: [
                    'article_id',
                ],
                where: {
                    'artist_id': artistIds,
                },
            }).map((obj)=>{
                 whereRule['id'].push(obj['article_id']);
            });
        }

        // logger.debug(artistIds);
        // logger.debug(relArtistArticle);

        let queryParam = {
            attributes: [
                'id',
                'title',
                'disp_state',
                'preview_pic_url',
                'abstract',
                'content',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let articles = await Article.findAndCountAll(queryParam);
            let count = articles.count;
            articles = articles.rows;

            articles = JSON.parse(JSON.stringify(articles));

            articles.map((obj)=>{
                return obj;
            });

            let articleIds = articles.map((obj)=>{
                return obj['id'];
            });
            let artistIds = [],
                relArticleArtist = {};
            await MergeArtistArticle.findAll({
                attributes: [
                    'artist_id',
                    'article_id',
                ],
                where: {
                    'article_id': articleIds,
                },
            }).map((obj)=>{
                artistIds.push(obj['artist_id']);
                if(!relArticleArtist[obj['article_id']]){
                    relArticleArtist[obj['article_id']] = [];
                }
                relArticleArtist[obj['article_id']].push(obj['artist_id']);
            });
            let artistMap = {};
            await Artist.findAll({
                attributes: [
                    'id',
                    'name',
                    'pic_url',
                ],
                where: {
                    'id': artistIds,
                },
            }).map((obj)=>{
                obj['pic_url'] = obj['pic_url'].split('|');
                artistMap[obj['id']] = obj;
            });

            articles.map((obj)=>{
               obj['artist_list'] = [];
               if(relArticleArtist[obj['id']]){
                   relArticleArtist[obj['id']].map((id)=> {
                       obj['artist_list'].push(artistMap[id]);
                   });
               }
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: articles,
                size: articles.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getArticleDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_article.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_article.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Article = getModel('article');
        const MergeArtistArticle = getModel('merge_artist_article');

        let queryParam = {
            attributes: [
                'id',
                'title',
                'disp_state',
                'preview_pic_url',
                'abstract',
                'content',
                'modifier',
                'modify_time',
                'create_time',
                'weight',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let article = await Article.findAll(queryParam);
            article = JSON.parse(JSON.stringify(article));

            if(article.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = article[0];

            let relsArticle = await MergeArtistArticle.findAll({
                attributes: [
                    'artist_id',
                ],
                where: {
                    'article_id': param['id'],
                },
            });
            data['artist_id'] = relsArticle.map((obj)=>{
                return obj['artist_id'];
            });

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
