const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports = {

    addProductCate: async function(req, res){
        let param = req.body;

        const ProductCate = getModel('product_cate');

        try {
            cu.checkFieldExists(param, 'name');
            cu.checkFieldExists(param, 'disp_state');
        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        param['id'] = uuidv1().replace(/-/g, '');

        param['modifier'] = ci.DEFAULT_MODIFIER;
        param['modify_time'] = Date.now();
        param['create_time'] = Date.now();

        let transaction = null;

        try {
            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let product_cate = await ProductCate.create(param, {transaction});
            param['id'] = product_cate['id'];

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: product_cate['id'],
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editProductCateDispState: async function(req, res){
        let param = req.body;

        const ProductCate = getModel('product_cate');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
            cu.checkFieldExists(param, 'disp_state');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let editParam = {};
        try {
            editParam['disp_state'] = param['disp_state'];
            editParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let product_cate = await ProductCate.update(editParam, {
                where: {
                    id: param['id'],
                },
                transaction,
            });

            if(product_cate[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    removeProductCate: async function(req, res){
        let param = req.body;

        const ProductCate = getModel('product_cate');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let delParam = {};
        try {
            delParam['lstate'] = enums.DataLState.DELETED;
            delParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let productCate = await ProductCate.update(delParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(productCate[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editProductCate: async function(req, res){
        let param = req.body;

        const ProductCate = getModel('product_cate');

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let productCate = await ProductCate.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(productCate[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getProductCate: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['disp_state', 'ASC'],
            ['weight', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_product_cate.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['query_name']){
            whereRule['$t_product_cate.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if(param['disp_state']){
            whereRule['$t_product_cate.disp_state$'] = {
                '$eq': param['disp_state'],
            }
        }

        let tsNow = moment().unix();

        const ProductCate = getModel('product_cate');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'preview_pic_url',
                'disp_state',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let productCate = await ProductCate.findAndCountAll(queryParam);
            let count = productCate.count;
            productCate = productCate.rows;
            productCate = JSON.parse(JSON.stringify(productCate));

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: productCate,
                size: productCate.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getProductCateDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_product_cate.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_product_cate.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const ProductCate = getModel('product_cate');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'preview_pic_url',
                'disp_state',
                'modifier',
                'modify_time',
                'create_time',
                'weight',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let product_cate = await ProductCate.findAll(queryParam);
            product_cate = JSON.parse(JSON.stringify(product_cate));

            if(product_cate.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = product_cate[0];

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
