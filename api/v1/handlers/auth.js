const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const crypto = require('crypto');

module.exports = {

    getSession: async function(req, res){
        return res.json({
            session: req.session,
            session_id: req.session.id,
            ret_code: ec.SUCCESS.code,
            ret_msg: ec.SUCCESS.message,
        });
    },

    getCurrentUser: async function(req, res){
        return res.json({
            userInfo: {
                name: req.session.userName,
                id: req.session.userId,
            },
            ret_code: ec.SUCCESS.code,
            ret_msg: ec.SUCCESS.message,
        });
    },

    loginPwd: async function(req, res){
        let param = req.body;

        let tNowSec = Math.ceil(new Date().getTime()/1000);

        let whereRule = {};

        whereRule['$t_admin_user.name$'] = {
            '$eq': param['account'],
        }
        whereRule['$t_admin_user.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const User = getModel('admin_user');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'portrait_url',
                'gender',
                'wechat_id',
                'phone',
                'enabled',
                'password',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['where'] = whereRule;
        let userObj = null;

        try {
            let user = await User.findAll(queryParam);
            user = JSON.parse(JSON.stringify(user));

            if(user.length < 1){
                return res.json({
                    ret_code: ec.ACCOUNT_PWD_ERROR.code,
                    ret_msg: ec.ACCOUNT_PWD_ERROR.message,
                });
            }

            userObj = user[0];
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

        // 校验密码
        let passwordHash = crypto.createHmac('sha1', param['password'] + userObj['id']).digest('hex');
        // logger.debug(param['password'] + userObj['id']);
        // logger.debug(passwordHash);
        if(userObj['password'] !== passwordHash){
            return res.json({
                ret_code: ec.ACCOUNT_PWD_ERROR.code,
                ret_msg: ec.ACCOUNT_PWD_ERROR.message,
            });
        }

        delete userObj['password'];

        if(!req.session){
            req.session = {};
        }

        //更新会话
        req.session.userId = userObj['id'];
        req.session.userName = userObj['name'];
        req.session.expireIn = tNowSec + 3600*24*7;
        req.session.user = userObj;

        res.json({
            ret_code: ec.SUCCESS.code,
            ret_msg: ec.SUCCESS.message,
        });
    },

    logout: async function(req, res){
        req.session.destroy((e)=>{
            if(e){
                logger.printExecption(e);
                return res.status(404).json({
                    ret_code: ec.FAILED.code,
                    ret_msg: ec.FAILED.message,
                });
            } else {
                res.json({
                    ret_code: ec.SUCCESS.code,
                    ret_msg: ec.SUCCESS.message,
                });
            }
        })
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
