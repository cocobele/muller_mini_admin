const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports = {

    addArtist: async function(req, res){
        let param = req.body;

        const Artist = getModel('artist');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const MergeArtistArticle = getModel('merge_artist_article');

        try {
            cu.checkFieldExists(param, 'name');
            cu.checkFieldExists(param, 'disp_state');
            cu.checkFieldExists(param, 'introduce');
            cu.checkFieldExists(param, 'pic_url');
            cu.checkFieldIsArray(param, 'pic_url');

            if(param['artwork_id']){
                cu.checkFieldIsArray(param, 'artwork_id');
            }

            if(param['article_id']){
                cu.checkFieldIsArray(param, 'article_id');
            }
        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        param['id'] = uuidv1().replace(/-/g, '');
        param['pic_url'] = param['pic_url'].join('|');

        param['modifier'] = ci.DEFAULT_MODIFIER;
        param['modify_time'] = Date.now();
        param['create_time'] = Date.now();

        let transaction = null;

        try {
            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artist = await Artist.create(param, {transaction});
            param['id'] = artist['id'];

            if(param['artwork_id']) {
                await MergeArtistArtwork.destroy({
                    where: {
                        artist_id: param['id'],
                    },
                    transaction,
                });

                let artistArtwork = [],
                    index = 0;
                for (let artworkId of param['artwork_id']) {
                    let relObj = {};
                    relObj['artist_id'] = param['id'];
                    relObj['artwork_id'] = artworkId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArtwork.push(relObj);
                }
                await MergeArtistArtwork.bulkCreate(artistArtwork, {transaction});
            }

            if(param['article_id']) {
                await MergeArtistArticle.destroy({
                    where: {
                        artist_id: param['id'],
                    },
                    transaction,
                });

                let artistArticle = [],
                    index = 0;
                for (let articleId of param['article_id']) {
                    let relObj = {};
                    relObj['artist_id'] = param['id'];
                    relObj['article_id'] = articleId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArticle.push(relObj);
                }
                await MergeArtistArticle.bulkCreate(artistArticle, {transaction});
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: artist['id'],
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editArtistDispState: async function(req, res){
        let param = req.body;

        const Artist = getModel('artist');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
            cu.checkFieldExists(param, 'disp_state');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let editParam = {};
        try {
            editParam['disp_state'] = param['disp_state'];
            editParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artist = await Artist.update(editParam, {
                where: {
                    id: param['id'],
                },
                transaction,
            });

            if(artist[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    removeArtist: async function(req, res){
        let param = req.body;

        const Artist = getModel('artist');
        const MergeArtistArticle = getModel('merge_artist_article');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const MergeExhibitionArtist = getModel('merge_exhibition_artist');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let delParam = {};
        try {
            delParam['lstate'] = enums.DataLState.DELETED;
            delParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artist = await Artist.update(delParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            await MergeArtistArticle.destroy({
                where: {
                    artist_id: param['id'],
                },
                transaction,
            });

            await MergeArtistArtwork.destroy({
                where: {
                    artist_id: param['id'],
                },
                transaction,
            });

            await MergeExhibitionArtist.destroy({
                where: {
                    artist_id: param['id'],
                },
                transaction,
            });

            if(artist[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editArtist: async function(req, res){
        let param = req.body;

        const Artist = getModel('artist');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const MergeArtistArticle = getModel('merge_artist_article');

        try {
            cu.checkFieldExists(param, 'id');

            if(param['pic_url']){
                cu.checkFieldIsArray(param, 'pic_url');
                param['pic_url'] = param['pic_url'].join('|');
            }
            if(param['artwork_id']){
                cu.checkFieldIsArray(param, 'artwork_id');
            }
            if(param['article_id']){
                cu.checkFieldIsArray(param, 'article_id');
            }
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artist = await Artist.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(artist[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            if(param['artwork_id']) {
                await MergeArtistArtwork.destroy({
                    where: {
                        artist_id: param['id'],
                    },
                    transaction,
                });

                let artistArtwork = [],
                    index = 0;
                for (let artworkId of param['artwork_id']) {
                    let relObj = {};
                    relObj['artist_id'] = param['id'];
                    relObj['artwork_id'] = artworkId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArtwork.push(relObj);
                }
                await MergeArtistArtwork.bulkCreate(artistArtwork, {transaction});
            }

            if(param['article_id']) {
                await MergeArtistArticle.destroy({
                    where: {
                        artist_id: param['id'],
                    },
                    transaction,
                });

                let artistArticle = [],
                    index = 0;
                for (let articleId of param['article_id']) {
                    let relObj = {};
                    relObj['artist_id'] = param['id'];
                    relObj['article_id'] = articleId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArticle.push(relObj);
                }
                await MergeArtistArticle.bulkCreate(artistArticle, {transaction});
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getArtist: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['disp_state', 'ASC'],
            ['weight', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_artist.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['query_name']){
            whereRule['$t_artist.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if(param['disp_state']){
            whereRule['$t_artist.disp_state$'] = {
                '$eq': param['disp_state'],
            }
        }

        let tsNow = moment().unix();

        const Artist = getModel('artist');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const MergeArtistArticle = getModel('merge_artist_article');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'pic_url',
                'introduce',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let artists = await Artist.findAndCountAll(queryParam);
            let count = artists.count;
            artists = artists.rows;
            artists = JSON.parse(JSON.stringify(artists));

            let artistIds =  artists.map((obj)=> {
                return obj['id'];
            });
            let countMap = {};
            let countArtwork = await MergeArtistArtwork.findAll({
                attributes: [
                    'artist_id',
                    [Sequelize.fn('COUNT', 'artist_id'), 'count']
                ],
                where: {
                    'artist_id': artistIds,
                },
                group: ['artist_id'],
            });
            let countArticle = await MergeArtistArticle.findAll({
                attributes: [
                    'artist_id',
                    [Sequelize.fn('COUNT', 'artist_id'), 'count']
                ],
                where: {
                    'artist_id': artistIds,
                },
                group: ['artist_id'],
            });
            countArticle = JSON.parse(JSON.stringify(countArticle));
            countArtwork = JSON.parse(JSON.stringify(countArtwork));

            for(let i = 0; i < artistIds.length; i++){
                countMap[artistIds[i]] = {
                    'article': 0,
                    'artwork': 0,
                }
            }
            countArticle.map((obj)=>{
                countMap[obj['artist_id']]['article'] = obj['count'];
            });
            countArtwork.map((obj)=>{
                countMap[obj['artist_id']]['artwork'] = obj['count'];
            });

            artists.map((obj)=>{
                obj['pic_url'] = obj['pic_url'].split('|');
                obj['artwork_num'] = countMap[obj['id']]['artwork']?countMap[obj['id']]['artwork']: 0;
                obj['article_num'] = countMap[obj['id']]['article']?countMap[obj['id']]['article']: 0;

                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: artists,
                size: artists.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getArtistDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_artist.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_artist.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Artist = getModel('artist');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'pic_url',
                'introduce',
                'modifier',
                'modify_time',
                'create_time',
                'weight',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let artist = await Artist.findAll(queryParam);
            artist = JSON.parse(JSON.stringify(artist));

            if(artist.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = artist[0];
            data['pic_url'] = data['pic_url'].split('|');

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
