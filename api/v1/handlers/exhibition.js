const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports = {

    addExhibition: async function(req, res){
        let param = req.body;

        const Exhibition = getModel('exhibition');
        const MergeExhibitionArtwork = getModel('merge_exhibition_artwork');
        const MergeExhibitionArtist = getModel('merge_exhibition_artist');

        try {
            cu.checkFieldExists(param, 'name');
            cu.checkFieldExists(param, 'disp_state');
            cu.checkFieldExists(param, 'preview_pic_url');
            cu.checkFieldExists(param, 'pic_url');
            cu.checkFieldIsArray(param, 'pic_url');
            cu.checkFieldExists(param, 'introduce');
            cu.checkFieldExists(param, 'open_time');
            cu.checkFieldIsDate(param, 'open_time');
            cu.checkFieldExists(param, 'close_time');
            cu.checkFieldIsDate(param, 'close_time');

            if(param['artist_id']){
                cu.checkFieldIsArray(param, 'artist_id');
            }
            if(param['artwork_id']){
                cu.checkFieldIsArray(param, 'artwork_id');
            }
        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        param['id'] = uuidv1().replace(/-/g, '');
        param['pic_url'] = param['pic_url'].join('|');

        param['modifier'] = ci.DEFAULT_MODIFIER;
        param['modify_time'] = Date.now();
        param['create_time'] = Date.now();

        let transaction = null;

        try {
            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let exhibition = await Exhibition.create(param, {transaction});
            param['id'] = exhibition['id'];

            if(param['artwork_id']) {
                await MergeExhibitionArtwork.destroy({
                    where: {
                        exhibition_id: param['id'],
                    },
                    transaction,
                });

                let exhibitionArtwork = [],
                    index = 0;
                for (let artworkId of param['artwork_id']) {
                    let relObj = {};
                    relObj['exhibition_id'] = param['id'];
                    relObj['artwork_id'] = artworkId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    exhibitionArtwork.push(relObj);
                }
                await MergeExhibitionArtwork.bulkCreate(exhibitionArtwork, {transaction});
            }

            if(param['artist_id']) {
                await MergeExhibitionArtist.destroy({
                    where: {
                        exhibition_id: param['id'],
                    },
                    transaction,
                });

                let exhibitionArtist = [],
                    index = 0;
                for (let artistId of param['artist_id']) {
                    let relObj = {};
                    relObj['exhibition_id'] = param['id'];
                    relObj['artist_id'] = artistId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    exhibitionArtist.push(relObj);
                }
                await MergeExhibitionArtist.bulkCreate(exhibitionArtist, {transaction});
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: exhibition['id'],
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editExhibitionDispState: async function(req, res){
        let param = req.body;

        const Exhibition = getModel('exhibition');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
            cu.checkFieldExists(param, 'disp_state');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let editParam = {};
        try {
            editParam['disp_state'] = param['disp_state'];
            editParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let exhibition = await Exhibition.update(editParam, {
                where: {
                    id: param['id'],
                },
                transaction,
            });

            if(exhibition[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    removeExhibition: async function(req, res){
        let param = req.body;

        const Exhibition = getModel('exhibition');
        const MergeExhibitionArtist = getModel('merge_exhibition_artist');
        const MergeExhibitionArtwork = getModel('merge_exhibition_artwork');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let delParam = {};
        try {
            delParam['lstate'] = enums.DataLState.DELETED;
            delParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let exhibition = await Exhibition.update(delParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            await MergeExhibitionArtist.destroy({
                where: {
                    exhibition_id: param['id'],
                },
                transaction,
            });

            await MergeExhibitionArtwork.destroy({
                where: {
                    exhibition_id: param['id'],
                },
                transaction,
            });

            if(exhibition[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editExhibition: async function(req, res){
        let param = req.body;

        const Exhibition = getModel('exhibition');
        const MergeExhibitionArtwork = getModel('merge_exhibition_artwork');
        const MergeExhibitionArtist = getModel('merge_exhibition_artist');

        try {
            cu.checkFieldExists(param, 'id');

            if(param['pic_url']){
                cu.checkFieldIsArray(param, 'pic_url');
                param['pic_url'] = param['pic_url'].join('|');
            }
            if(param['artwork_id']){
                cu.checkFieldIsArray(param, 'artwork_id');
            }
            if(param['artist_id']){
                cu.checkFieldIsArray(param, 'artist_id');
            }
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let exhibition = await Exhibition.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(exhibition[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            if(param['artwork_id']) {
                await MergeExhibitionArtwork.destroy({
                    where: {
                        exhibition_id: param['id'],
                    },
                    transaction,
                });

                let exhibitionArtwork = [],
                    index = 0;
                for (let artworkId of param['artwork_id']) {
                    let relObj = {};
                    relObj['exhibition_id'] = param['id'];
                    relObj['artwork_id'] = artworkId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    exhibitionArtwork.push(relObj);
                }
                await MergeExhibitionArtwork.bulkCreate(exhibitionArtwork, {transaction});
            }

            if(param['artist_id']) {
                await MergeExhibitionArtist.destroy({
                    where: {
                        exhibition_id: param['id'],
                    },
                    transaction,
                });

                let exhibitionArtist = [],
                    index = 0;
                for (let artistId of param['artist_id']) {
                    let relObj = {};
                    relObj['exhibition_id'] = param['id'];
                    relObj['artist_id'] = artistId;
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    exhibitionArtist.push(relObj);
                }
                await MergeExhibitionArtist.bulkCreate(exhibitionArtist, {transaction});
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getExhibition: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['disp_state', 'ASC'],
            ['weight', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_exhibition.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['query_name']){
            whereRule['$t_exhibition.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if(param['disp_state']){
            whereRule['$t_exhibition.disp_state$'] = {
                '$eq': param['disp_state'],
            }
        }

        if(param['open_time']){
            whereRule['$t_exhibition.open_time$'] = {
                '$gt': param['open_time'],
            }
        }

        if(param['close_time']){
            whereRule['$t_exhibition.close_time$'] = {
                '$lt': param['close_time'],
            }
        }

        let tsNow = moment().unix();

        const Exhibition = getModel('exhibition');
        const MergeExhibitionArtwork = getModel('merge_exhibition_artwork');
        const MergeExhibitionArtist = getModel('merge_exhibition_artist');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'preview_pic_url',
                'pic_url',
                'introduce',
                'open_time',
                'close_time',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let exhibitions = await Exhibition.findAndCountAll(queryParam);
            let count = exhibitions.count;
            exhibitions = exhibitions.rows;
            exhibitions = JSON.parse(JSON.stringify(exhibitions));

            let exhibitionIds =  exhibitions.map((obj)=> {
                return obj['id'];
            });
            let countMap = {};
            let countArtwork = await MergeExhibitionArtwork.count({
                attributes: [
                    'exhibition_id',
                    [Sequelize.fn('COUNT', 'exhibition_id'), 'count']
                ],
                where: {
                    'exhibition_id': exhibitionIds,
                },
                group: ['exhibition_id'],
            });

            for(let i = 0; i < exhibitionIds.length; i++){
                countMap[exhibitionIds[i]] = {
                    'artwork': 0,
                }
            }
            countArtwork.map((obj)=>{
                countMap[obj['exhibition_id']]['artwork'] = obj['count'];
            });
            // logger.debug(countMap);

            exhibitions.map((obj)=>{
                obj['pic_url'] = obj['pic_url'].split('|');
                obj['artwork_num'] = countMap[obj['id']]['artwork'];

                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: exhibitions,
                size: exhibitions.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getExhibitionDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_exhibition.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_exhibition.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Exhibition = getModel('exhibition');
        const MergeExhibitionArtwork = getModel('merge_exhibition_artwork');
        const MergeExhibitionArtist = getModel('merge_exhibition_artist');
        const Artwork = getModel('artwork');
        const Artist = getModel('artist');
        const CommodityStock = getModel('commodity_stock');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'preview_pic_url',
                'pic_url',
                'introduce',
                'open_time',
                'close_time',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
                'weight',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let exhibition = await Exhibition.findAll(queryParam);
            exhibition = JSON.parse(JSON.stringify(exhibition));

            if(exhibition.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = exhibition[0];
            data['pic_url'] = data['pic_url'].split('|');

            let relArtworks = await MergeExhibitionArtwork.findAll({
                where: {
                    exhibition_id: param['id'],
                },
            });
            let artworkIds = relArtworks.map((obj)=>{
                return obj['artwork_id'];
            });
            let artworks = await Artwork.findAll({
                where: {
                    id: artworkIds,
                },
            });
            artworks = JSON.parse(JSON.stringify(artworks));
            data['artwork'] = artworks;

            let commodityStock = await CommodityStock.findAll({
                attributes: [
                    'commodity_id',
                    'fee',
                ],
                where: {
                    'commodity_id': artworkIds,
                    'commodity_type': enums.CommodityType.ARTWORK,
                    'lstate': enums.DataLState.ACTIVE,
                },
            });

            let stockFeeMap = {};
            commodityStock.map((obj)=>{
                stockFeeMap[obj['commodity_id']] = obj;
            })

            data['artwork'].map((obj)=>{
                if(stockFeeMap[obj['id']]){
                    obj['commodity_sale_fee'] = stockFeeMap[obj['id']]['fee'];
                }
            })

            let relArtists = await MergeExhibitionArtist.findAll({
                where: {
                    exhibition_id: param['id'],
                },
            });
            let artistIds = relArtists.map((obj)=>{
                return obj['artist_id'];
            });
            let artists = await Artist.findAll({
                where: {
                    id: artistIds,
                },
            });
            data['artist'] = artists;

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
