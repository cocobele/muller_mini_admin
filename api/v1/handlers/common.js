const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const crypto = require('crypto');
const gm = require('gm');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const oss = require('ali-oss').Wrapper;
const Promise = require('bluebird');

module.exports = {

    getOSSAuth: function(req, res){
        let param = req.body;

        let expiration = new Date();     // 指定policy过期时间
        let conditions = [];
        let signatureObj = {};           // 待签名对象

        let cfg = globalContext.config;

        let ossId = cfg['aliyun']['access_keys']['id'],
            ossSecret = cfg['aliyun']['access_keys']['secret'],
            ossHost = cfg['oss']['yanshan-upload']['host'];

        let dirInd = Math.ceil(cu.timestampNow() / (3600*7*24));
        let dir = cfg['oss']['yanshan-upload']['dir_base'] + '/attach/' + dirInd;

        // UTC默认是不会加上当前时区 orz
        expiration.setHours(expiration.getHours() + 8);
        expiration.setSeconds(expiration.getSeconds() + 10);
        conditions.push(['content-length-range', 0, 1048576000]); // 文件大小
        conditions.push(['starts-with', '$key', dir]);  // 校验目录
        signatureObj.expiration = expiration.toISOString();  // 使用ISO格式日期
        signatureObj.conditions = conditions;
        let base64Policy = new Buffer(JSON.stringify(signatureObj)).toString('base64');
        // 创建带有secret秘钥的哈希值
        let signature = crypto.createHmac('sha1', ossSecret).update(base64Policy).digest().toString('base64');
        if(!base64Policy || !signature) {
            return res.status(404).json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
        return res.json({
            ret_code: ec.SUCCESS.code,
            ret_msg: ec.SUCCESS.message,
            accessId: ossId,
            host: ossHost,
            policy: base64Policy,
            signature: signature,
            expire: +expiration,
            dir: dir
        })
    },

    uploadImage: async function(req, res){
        let imgFile = req.file;
        if (!imgFile) {
            return res.status(404).json({
                ret_code: ec.CANNOT_FIND_UPLOADED_FILE.code,
                ret_msg: ec.CANNOT_FIND_UPLOADED_FILE.message,
            });
        }

        // logger.debug([imgFile['originalname'], imgFile['mimetype'], imgFile['buffer'].length]);

        let param = req.body,
            config = globalContext.config;

        const Image = getModel('image');

        let ossClient = new oss({
            region: config['oss']['region'],
            accessKeyId: config['aliyun']['access_keys']['id'],
            accessKeySecret: config['aliyun']['access_keys']['secret'],
            bucket: config['oss']['yanshan-upload']['name'],
        });

        // let fGM = gm(imgFile['buffer'], imgFile['originalname']);
        // let size = await fGM.size();
        // logger.debug(size);

        let objKey = cu.sha256(imgFile['originalname'] + cu.timestampNow());
        let dirInd = Math.ceil(cu.timestampNow() / (3600*7*24));
        let dir =  config['oss']['yanshan-upload']['dir_base'] + '/pic/' + dirInd;

        let picUrl = null;
        try {
            let ossKey = dir + '/' + objKey;
            let putRes = await ossClient.put(ossKey, imgFile['buffer']);
            // logger.debug(putRes['url']);
            picUrl = config['oss']['yanshan-upload']['cdn_url'] + ossKey;
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

        let imgObj = {};
        imgObj['raw_url'] = picUrl;
        imgObj['file_url'] = picUrl;
        imgObj['width'] = -1; //size['width'];
        imgObj['height'] = -1; //size['height'];
        imgObj['mime'] = imgFile['mimetype'];
        imgObj['file_size'] = imgFile['buffer'].length;
        imgObj['modifier'] = ci.DEFAULT_MODIFIER;
        imgObj['modify_time'] = Date.now();
        imgObj['create_time'] = Date.now();

        try {
            let image = await Image.create(imgObj);

            return res.json(_.extend({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: image['id'],
            }, imgObj));
        } catch (e) {
            //删除图片
            await ossClient.delete(dir + '/' + objKey);

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getAppProfile: async function(req, res) {
        let param = req.query;

        let whereRule = {};

        whereRule['$t_app_config.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const AppConfig = getModel('app_config');

        let queryParam = {
            attributes: [
                'id',
                'val',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let profiles = await AppConfig.findAll(queryParam);
            profiles = JSON.parse(JSON.stringify(profiles));

            let profile = {},
                data = {};

            profiles.map((obj)=>{
                profile[obj['id']] = obj['val'];
            });

            data['profile'] = profile;

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
