const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports = {

    addProduct: async function(req, res){
        let param = req.body;

        const Product = getModel('product');
        const CommodityStock = getModel('commodity_stock');

        try {
            cu.checkFieldExists(param, 'name');
            cu.checkFieldExists(param, 'disp_state');
            cu.checkFieldExists(param, 'preview_pic_url');
            cu.checkFieldExists(param, 'list_view_pic_url');
            cu.checkFieldExists(param, 'description');
            cu.checkFieldExists(param, 'pic_url');
            cu.checkFieldIsArray(param, 'pic_url');

            cu.checkFieldExists(param, 'brand_id');
            cu.checkFieldIsArray(param, 'brand_id');

            cu.checkFieldExists(param, 'product_cate_id');
            cu.checkFieldIsArray(param, 'product_cate_id');

            if(param['commodity_stock']){
                cu.checkFieldIsArray(param, 'commodity_stock');
            }
        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        param['id'] = uuidv1().replace(/-/g, '');
        param['pic_url'] = param['pic_url'].join('|');
        param['brand_id'] = param['brand_id'].join('|');
        param['product_cate_id'] = param['product_cate_id'].join('|');

        param['modifier'] = ci.DEFAULT_MODIFIER;
        param['modify_time'] = Date.now();
        param['create_time'] = Date.now();

        let transaction = null;

        try {
            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let product = await Product.create(param, {transaction});
            param['id'] = product['id'];

            if(param['commodity_stock']){
                for(let i = 0; i < param['commodity_stock'].length; i++){
                    let stockParam = {},
                        obj = param['commodity_stock'][i];
                    stockParam['id'] = uuidv1().replace(/-/g, '');
                    stockParam['commodity_id'] = param['id'];
                    stockParam['commodity_type'] = enums.CommodityType.PRODUCT;
                    stockParam['spec_name'] = obj['spec_name'];
                    stockParam['fee'] = obj['fee'];
                    stockParam['fee_type'] = enums.CurrencyType.CNY;
                    stockParam['num'] = obj['num'];
                    stockParam['weight'] = obj['weight'];
                    stockParam['disp_state'] = enums.DispState.UP;
                    stockParam['modifier'] = ci.DEFAULT_MODIFIER;
                    stockParam['modify_time'] = Date.now();
                    stockParam['create_time'] = Date.now();

                    if(obj['preview_pic_url']){
                        stockParam['preview_pic_url'] = obj['preview_pic_url'].join('|');
                    }

                    let stock = await CommodityStock.create(stockParam, {transaction});
                }
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: product['id'],
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editProductDispState: async function(req, res){
        let param = req.body;

        const Product = getModel('product');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
            cu.checkFieldExists(param, 'disp_state');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let editParam = {};
        try {
            editParam['disp_state'] = param['disp_state'];
            editParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let product = await Product.update(editParam, {
                where: {
                    id: param['id'],
                },
                transaction,
            });

            if(product[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    removeProduct: async function(req, res){
        let param = req.body;

        const Product = getModel('product');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let delParam = {};
        try {
            delParam['lstate'] = enums.DataLState.DELETED;
            delParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let product = await Product.update(delParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(product[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editProduct: async function(req, res){
        let param = req.body;

        const Product = getModel('product');
        const CommodityStock = getModel('commodity_stock');

        try {
            cu.checkFieldExists(param, 'id');

            if(param['pic_url']){
                cu.checkFieldIsArray(param, 'pic_url');
                param['pic_url'] = param['pic_url'].join('|');
            }
            if(param['brand_id']){
                cu.checkFieldIsArray(param, 'brand_id');
                param['brand_id'] = param['brand_id'].join('|');
            }
            if(param['product_cate_id']){
                cu.checkFieldIsArray(param, 'product_cate_id');
                param['product_cate_id'] = param['product_cate_id'].join('|');
            }
            if(param['commodity_stock']){
                cu.checkFieldIsArray(param, 'commodity_stock');
            }
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let product = await Product.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(product[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            if(param['commodity_stock']) {
                let stockParam = {};
                stockParam['modifier'] = ci.DEFAULT_MODIFIER;
                stockParam['modify_time'] = Date.now();
                stockParam['create_time'] = Date.now();
                stockParam['lstate'] = enums.DataLState.DELETED;

                await CommodityStock.update(stockParam, {
                    where: {
                        commodity_id: param['id'],
                        commodity_type: enums.CommodityType.PRODUCT,
                    },
                    transaction,
                });

                for (let i = 0; i < param['commodity_stock'].length; i++) {
                    let stockParam = {},
                        obj = param['commodity_stock'][i];

                    stockParam['commodity_id'] = param['id'];
                    stockParam['commodity_type'] = enums.CommodityType.PRODUCT;
                    stockParam['spec_name'] = obj['spec_name'];
                    stockParam['fee'] = obj['fee'];
                    stockParam['fee_type'] = enums.CurrencyType.CNY;
                    stockParam['num'] = obj['num'];
                    stockParam['weight'] = obj['weight'];
                    stockParam['disp_state'] = obj['disp_state'];
                    stockParam['lstate'] = enums.DataLState.ACTIVE;

                    if(obj['preview_pic_url']){
                        stockParam['preview_pic_url'] = obj['preview_pic_url'].join('|');
                    }

                    if(obj['id']){
                        let stock = await CommodityStock.update(stockParam, {
                            where: {
                                id: obj['id'],
                            },
                            transaction,
                        });
                    } else {
                        stockParam['id'] = uuidv1().replace(/-/g, '');
                        let stock = await CommodityStock.create(stockParam, {transaction});
                    }

                }
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getProduct: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['disp_state', 'ASC'],
            ['weight', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_product.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['query_name']){
            whereRule['$t_product.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if(param['disp_state']){
            whereRule['$t_product.disp_state$'] = {
                '$eq': param['disp_state'],
            }
        }

        if(param['brand_id']){
            whereRule['$t_product.brand_id$'] = {
                '$like': '%' + param['brand_id'] + '%',
            }
        }

        if(param['product_cate_id']){
            whereRule['$t_product.product_cate_id$'] = {
                '$like': '%' + param['product_cate_id'] + '%',
            }
        }

        let tsNow = moment().unix();

        const Product = getModel('product');
        const Brand = getModel('brand');
        const ProductCate = getModel('product_cate');
        const CommodityStock = getModel('commodity_stock');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'brand_id',
                'product_cate_id',
                'preview_pic_url',
                'list_view_pic_url',
                'pic_url',
                'introduce',
                'description',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let products = await Product.findAndCountAll(queryParam);
            let count = products.count;
            products = products.rows;
            products = JSON.parse(JSON.stringify(products));

            let productIds = products.map((obj)=> {
                obj['pic_url'] = obj['pic_url'].split('|');
                obj['brand_id'] = obj['brand_id'].split('|');
                obj['product_cate_id'] = obj['product_cate_id'].split('|');

                return obj['id'];
            });

            let brandMap = {},
                productCateMap = {};

            let brands = await Brand.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                },
            });
            let productCates = await ProductCate.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                },
            });

            brands.map((obj)=> {
                brandMap[obj['id']] = obj;
            });
            productCates.map((obj)=> {
                productCateMap[obj['id']] = obj;
            });

            // logger.debug(brands);
            // logger.debug(brandMap);

            let commodityStocks = await CommodityStock.findAll({
                attributes: [
                    'commodity_id',
                    'spec_name',
                    'fee',
                    'num',
                    'weight',
                    'disp_state',
                ],
                where: {
                    'commodity_id': productIds,
                    'commodity_type': enums.CommodityType.PRODUCT,
                    'lstate': enums.DataLState.ACTIVE,
                },
            });

            let commodityStocksMap = {};
            commodityStocks.map((obj)=>{
                if(!commodityStocksMap[obj['commodity_id']]){
                    commodityStocksMap[obj['commodity_id']] = [];
                }

                commodityStocksMap[obj['commodity_id']].push(obj);
                delete obj['commodity_id'];
            });

            products.map((obj)=>{
                obj['product_cate'] = [];
                obj['brand'] = [];

                logger.debug(obj['brand_id']);
                logger.debug(brandMap);

                obj['brand_id'].map((i)=> {
                    obj['brand'].push(brandMap[i]);
                });
                obj['product_cate_id'].map((i)=> {
                    obj['product_cate'].push(productCateMap[i]);
                });

                if(commodityStocksMap[obj['id']]){
                    obj['commodity_stock'] = commodityStocksMap[obj['id']];
                } else {
                    obj['commodity_stock'] = [];
                }

                delete obj['brand_id'];
                delete obj['product_cate_id'];

                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: products,
                size: products.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getProductDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_product.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_product.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Product = getModel('product');
        const Brand = getModel('brand');
        const ProductCate = getModel('product_cate');
        const CommodityStock = getModel('commodity_stock');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'brand_id',
                'product_cate_id',
                'preview_pic_url',
                'list_view_pic_url',
                'pic_url',
                'introduce',
                'description',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
                'weight',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let product = await Product.findAll(queryParam);
            product = JSON.parse(JSON.stringify(product));

            if(product.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = product[0];
            data['pic_url'] = data['pic_url'].split('|');
            data['product_cate_id'] = data['product_cate_id'].split('|');
            data['brand_id'] = data['brand_id'].split('|');

            data['brand'] = await Brand.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                    'id': data['brand_id'],
                },
            });
            data['product_cate'] = await ProductCate.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                    'id': data['product_cate_id'],
                },
            });

            let commodityStocks = await CommodityStock.findAll({
                attributes: [
                    'id',
                    'spec_name',
                    'fee',
                    'num',
                    'weight',
                    'disp_state',
                    'preview_pic_url',
                ],
                where: {
                    'commodity_id': param['id'],
                    'commodity_type': enums.CommodityType.PRODUCT,
                    'lstate': enums.DataLState.ACTIVE,
                },
            });
            commodityStocks = JSON.parse(JSON.stringify(commodityStocks));
            data['commodity_stock'] = commodityStocks;

            data['commodity_stock'].map((obj)=>{
                if(obj['preview_pic_url']){
                    obj['preview_pic_url'] = obj['preview_pic_url'].split('|');
                } else {
                    obj['preview_pic_url'] = [];
                }
            });

            delete data['product_cate_id'];
            delete data['brand_id'];

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
