const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const crypto = require('crypto');

module.exports = {

    addUser: async function(req, res){
        let param = req.body;

        const User = getModel('admin_user');

        try {
            cu.checkFieldExists(param, 'name');
            cu.checkFieldExists(param, 'phone');
            cu.checkFieldExists(param, 'password');
        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        param['modifier'] = ci.DEFAULT_MODIFIER;
        param['modify_time'] = Date.now();
        param['create_time'] = Date.now();

        let transaction = null;

        try {
            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let user = await User.create(param, {transaction});
            logger.debug(user);
            let passwordHash = crypto.createHmac('sha1', param['password'] + user['id']).digest('hex');
            let userUpdateParam = {
                'password': passwordHash,
            };
            await User.update(userUpdateParam, {
                where: {
                    id: user['id'],
                },
                transaction,
            });

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: user['id'],
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    removeUser: async function(req, res){
        let param = req.body;

        const User = getModel('admin_user');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let delParam = {};
        try {
            delParam['lstate'] = enums.DataLState.DELETED;
            delParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let user = await User.update(delParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(user[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editUser: async function(req, res){
        let param = req.body;

        const User = getModel('admin_user');

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            if(param['password']){
                param['password'] = crypto.createHmac('sha1', param['password'] + param['id']).digest('hex');
            }

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let user = await User.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(user[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getUser: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_admin_user.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['id']){
            whereRule['$t_admin_user.id$'] = {
                '$eq': param['id'],
            }
        }
        if(param['query_name']){
            whereRule['$t_admin_user.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if(param['enabled']){
            whereRule['$t_admin_user.enabled$'] = {
                '$eq': param['enabled'],
            }
        }

        const User = getModel('admin_user');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'portrait_url',
                'gender',
                'wechat_id',
                'phone',
                'enabled',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let users = await User.findAndCountAll(queryParam);
            let count = users.count;
            users = users.rows;

            users.map((obj)=>{
                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: users,
                size: users.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getUserDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_admin_user.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_admin_user.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const User = getModel('admin_user');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'portrait_url',
                'gender',
                'wechat_id',
                'phone',
                'enabled',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let user = await User.findAll(queryParam);
            user = JSON.parse(JSON.stringify(user));

            if(user.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = user[0];

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
