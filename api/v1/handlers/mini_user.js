const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const crypto = require('crypto');

module.exports = {

    getMiniUser: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_mini_user.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['id']){
            whereRule['$t_mini_user.id$'] = {
                '$eq': param['id'],
            }
        }

        if(param['query_name']){
            whereRule['$t_mini_user.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if(param['query_phone']){
            whereRule['$t_mini_user.phone$'] = {
                '$like': '%' + param['query_phone'] + '%',
            }
        }

        const MiniUser = getModel('mini_user');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'logo',
                'gender',
                'open_id',
                'address',
                'phone',
                'last_login_time',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let users = await MiniUser.findAndCountAll(queryParam);
            let count = users.count;
            users = users.rows;

            users.map((obj)=>{
                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: users,
                size: users.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
