const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports = {

    editB2COrder: async function(req, res){
        let param = req.body;

        const B2COrder = getModel('b2c_order');

        try {
            cu.checkFieldExists(param, 'id');

            let paramNew = {};
            if(param['remark']){
                paramNew['remark'] = param['remark'];
            }
            if(param['contact_name']){
                paramNew['contact_name'] = param['contact_name'];
            }
            if(param['contact_addr']){
                paramNew['contact_addr'] = param['contact_addr'];
            }
            if(param['contact_phone']){
                paramNew['contact_phone'] = param['contact_phone'];
            }
            if(param['express_name']){
                paramNew['express_name'] = param['express_name'];
            }
            if(param['express_order_id']){
                paramNew['express_order_id'] = param['express_order_id'];
            }
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let order = await B2COrder.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(order[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editB2COrderTradeState: async function(req, res){
        let param = req.body;

        const B2COrder = getModel('b2c_order');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldExists(param, 'new_state');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        // 检查状态流转逻辑
        let order = await B2COrder.findAll({
            attributes: [
                'id',
                'trade_state',
            ],
            where: {
                'id': param['id'],
            },
        });
        if(order.length < 1){
            return res.json({
                ret_code: ec.RES_NOT_EXISTS.code,
                ret_msg: ec.RES_NOT_EXISTS.message,
            });
        }
        order = order[0];

        if(order['trade_state'] == enums.B2cOrderState.USER_PAID){
            // 当前订单已支付
            if(param['new_state'] != enums.B2cOrderState.SHIPPED &&
                param['new_state'] != enums.B2cOrderState.REFUNDING){
                return res.json({
                    ret_code: ec.ORDER_TRADE_STATE_UPDATE_NOT_ALLOWED.code,
                    ret_msg: ec.ORDER_TRADE_STATE_UPDATE_NOT_ALLOWED.message,
                });
            }
        } else if(order['trade_state'] == enums.B2cOrderState.SHIPPED){
            // 当前订单已发货
            if(param['new_state'] != enums.B2cOrderState.SUCCESS &&
                param['new_state'] != enums.B2cOrderState.REFUNDING){
                return res.json({
                    ret_code: ec.ORDER_TRADE_STATE_UPDATE_NOT_ALLOWED.code,
                    ret_msg: ec.ORDER_TRADE_STATE_UPDATE_NOT_ALLOWED.message,
                });
            }
        } else {
            return res.json({
                ret_code: ec.ORDER_TRADE_STATE_UPDATE_NOT_ALLOWED.code,
                ret_msg: ec.ORDER_TRADE_STATE_UPDATE_NOT_ALLOWED.message,
            });
        }

        let updateParam = {};
        updateParam['trade_state'] = param['new_state'];

        if(param['remark']){
            updateParam['remark'] = param['remark'];
        }

        try {
            updateParam['modifier'] = ci.DEFAULT_MODIFIER;
            updateParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let order = await B2COrder.update(updateParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(order[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getB2COrder: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['time_start', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_b2c_order.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['query_commodity_name']){
            whereRule['$t_b2c_order.commodity_name$'] = {
                '$like': '%' + param['query_commodity_name'] + '%',
            }
        }

        if(param['query_contact_name']){
            whereRule['$t_b2c_order.contact_name$'] = {
                '$like': '%' + param['query_contact_name'] + '%',
            }
        }

        if(param['b2c_order_id']){
            whereRule['$t_b2c_order.id$'] = {
                '$eq': param['b2c_order_id'],
            }
        }

        if(param['trade_state']){
            whereRule['$t_b2c_order.trade_state$'] = {
                '$eq': param['trade_state'],
            }
        }

        if(param['time_start_begin']){
            whereRule['$t_b2c_order.time_start$'] = {
                '$gt': param['time_start_begin'],
            }
        }

        if(param['time_start_end']){
            whereRule['$t_b2c_order.time_start$'] = {
                '$lt': param['time_start_end'],
            }
        }

        let tsNow = moment().unix();

        const B2COrder = getModel('b2c_order');
        const orderStock = getModel('order_stock');


        let queryParam = {
            attributes: [
                'id',
                'commodity_name',
                'total_fee',
                'cash_fee',
                'num',
                'time_start',
                'time_pay',
                'time_end',
                'trade_state',
                'trade_state_desc',
                'remark',
                'contact_name',
                'contact_phone',
                'pay_channel_type',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let orders = await B2COrder.findAndCountAll(queryParam);
            let count = orders.count;
            orders = orders.rows;
            orders = JSON.parse(JSON.stringify(orders));
            let orderIds =  orders.map((obj)=> {
                return obj['id'];
            });
            let stocks=await this.GetOrderStock(orderIds);
            console.log('orders',orders)
            orders.map((obj)=>{
                console.log(obj)
                obj.stocks=stocks[obj.id]
                let count=0
                obj.stocks&&obj.stocks.map((one)=>{
                    count+=one.num
                })
                obj.num=count;
                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: orders,
                size: orders.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getB2COrderDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_b2c_order.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_b2c_order.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const B2COrder = getModel('b2c_order');
        const B2COrderRoll = getModel('b2c_order_roll');

        let queryParam = {
            attributes: [
                'id',
                'commodity_name',
                'total_fee',
                'cash_fee',
                'num',
                'time_start',
                'time_pay',
                'time_end',
                'trade_state',
                'trade_state_desc',
                'remark',
                'contact_name',
                'contact_phone',
                'pay_channel_type',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let order = await B2COrder.findAll(queryParam);
            order = JSON.parse(JSON.stringify(order));

            if(order.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = order[0];

            let roll = await B2COrderRoll.findAll({
                attributes: [
                    'trade_state',
                    'trade_state_desc',
                    'modify_time',
                    'modifier',
                ],
                where: {
                    'b2c_order_id': param['id'],
                },
                order: [
                    ['modify_time', 'DESC']
                ]
            });

            data['order_roll'] = roll;
            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            let stocks=await this.GetOrderStock([param['id']]);
            data['stocks']=stocks?stocks[param['id']]:[];
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },
   async GetOrderStock(ids){
        const orderStock = getModel('order_stock')
        let queryParam = {
            attributes:["order_id","stock_id","name","spec_name","preview_pic_url","fee","num"],
            where:{
                "order_id":ids
            }
        }
        let stock = await orderStock.findAll(queryParam);
        stock = JSON.parse(JSON.stringify(stock));
        let result = {}
        stock.forEach(element => {
            result[element['order_id']]=result[element['order_id']]||[]
            result[element['order_id']].push(element)
        });
        console.log('GetOrderStock',result)
        return result
    }


};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
