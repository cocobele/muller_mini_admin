const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports = {

    addArtwork: async function(req, res){
        let param = req.body;

        const Artwork = getModel('artwork');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const CommodityStock = getModel('commodity_stock');

        try {
            cu.checkFieldExists(param, 'name');
            // cu.checkFieldIsArray(param, 'artist_name');
            cu.checkFieldExists(param, 'disp_state');
            cu.checkFieldExists(param, 'preview_pic_url');
            // cu.checkFieldExists(param, 'introduce');
            cu.checkFieldExists(param, 'description');
            // cu.checkFieldIsArray(param, 'audio_guide_id');
            cu.checkFieldExists(param, 'pic_url');
            cu.checkFieldIsArray(param, 'pic_url');

            if(param['artist_id']){
                cu.checkFieldIsArray(param, 'artist_id');
            }

            if(param['commodity_sale_type'] && param['commodity_sale_type'] == enums.CommoditySaleType.ONLINE){
                cu.checkFieldExists(param, 'commodity_sale_fee');
            }

            if(param['spec_info']){
                cu.checkFieldIsArray(param, 'spec_info');
                param['spec_info'] = JSON.stringify(param['spec_info']);
            }
        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        param['id'] = uuidv1().replace(/-/g, '');
        param['pic_url'] = param['pic_url'].join('|');

        param['modifier'] = ci.DEFAULT_MODIFIER;
        param['modify_time'] = Date.now();
        param['create_time'] = Date.now();

        let transaction = null;

        try {
            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artwork = await Artwork.create(param, {transaction});
            param['id'] = artwork['id'];

            if(param['artist_id']) {
                await MergeArtistArtwork.destroy({
                    where: {
                        artwork_id: param['id'],
                    },
                    transaction,
                });

                let artistArtwork = [],
                    index = 0;
                for (let artistId of param['artist_id']) {
                    let relObj = {};
                    relObj['artist_id'] = artistId;
                    relObj['artwork_id'] = param['id'];
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArtwork.push(relObj);
                }
                await MergeArtistArtwork.bulkCreate(artistArtwork, {transaction});
            }

            if(param['commodity_sale_type'] == enums.CommoditySaleType.ONLINE){
                let stockParam = {};

                stockParam['id'] = uuidv1().replace(/-/g, '');
                stockParam['commodity_id'] = param['id'];
                stockParam['commodity_type'] = enums.CommodityType.ARTWORK;
                stockParam['spec_name'] = 'default';
                stockParam['fee'] = param['commodity_sale_fee'];
                stockParam['fee_type'] = enums.CurrencyType.CNY;
                stockParam['num'] = 1;
                stockParam['disp_state'] = enums.DispState.UP;
                stockParam['modifier'] = ci.DEFAULT_MODIFIER;
                stockParam['modify_time'] = Date.now();
                stockParam['create_time'] = Date.now();

                let stock = await CommodityStock.create(stockParam, {transaction});
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                id: artwork['id'],
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editArtworkDispState: async function(req, res){
        let param = req.body;

        const Artwork = getModel('artwork');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
            cu.checkFieldExists(param, 'disp_state');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let editParam = {};
        try {
            editParam['disp_state'] = param['disp_state'];
            editParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artwork = await Artwork.update(editParam, {
                where: {
                    id: param['id'],
                },
                transaction,
            });

            if(artwork[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    removeArtwork: async function(req, res){
        let param = req.body;

        const Artwork = getModel('artwork');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const MergeExhibitionArtwork = getModel('merge_exhibition_artwork');

        try {
            cu.checkFieldExists(param, 'id');
            cu.checkFieldIsArray(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        let delParam = {};
        try {
            delParam['lstate'] = enums.DataLState.DELETED;
            delParam['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artwork = await Artwork.update(delParam, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            await MergeArtistArtwork.destroy({
                where: {
                    artwork_id: param['id'],
                },
                transaction,
            });

            await MergeArtistArtwork.destroy({
                where: {
                    artwork_id: param['id'],
                },
                transaction,
            });

            await MergeExhibitionArtwork.destroy({
                where: {
                    artwork_id: param['id'],
                },
                transaction,
            });

            if(artwork[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    editArtwork: async function(req, res){
        let param = req.body;

        const Artwork = getModel('artwork');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const CommodityStock = getModel('commodity_stock');

        try {
            cu.checkFieldExists(param, 'id');

            if(param['artist_id']){
                cu.checkFieldIsArray(param, 'artist_id');
            }

            if(param['pic_url']){
                cu.checkFieldIsArray(param, 'pic_url');
                param['pic_url'] = param['pic_url'].join('|');
            }

            if(param['commodity_sale_type'] && param['commodity_sale_type'] == enums.CommoditySaleType.ONLINE){
                cu.checkFieldExists(param, 'commodity_sale_fee');
            }

            if(param['spec_info']){
                cu.checkFieldIsArray(param, 'spec_info');
                param['spec_info'] = JSON.stringify(param['spec_info']);
            }
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let transaction = null;

        try {
            param['modifier'] = ci.DEFAULT_MODIFIER;
            param['modify_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();

            let artwork = await Artwork.update(param, {
                where: {
                    id: param['id'],
                    lstate: enums.DataLState.ACTIVE,
                },
                transaction,
            });

            if(artwork[0] == 0){
                await transaction.rollback();

                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            if(param['artist_id']) {
                await MergeArtistArtwork.destroy({
                    where: {
                        artwork_id: param['id'],
                    },
                    transaction,
                });

                let artistArtwork = [],
                    index = 0;
                for (let artistId of param['artist_id']) {
                    let relObj = {};
                    relObj['artist_id'] = artistId;
                    relObj['artwork_id'] = param['id'];
                    relObj['sort'] = index++;
                    relObj['lstate'] = enums.DataLState.ACTIVE;
                    relObj['modifier'] = ci.DEFAULT_MODIFIER;
                    relObj['modify_time'] = Date.now();
                    relObj['create_time'] = Date.now();
                    artistArtwork.push(relObj);
                }
                await MergeArtistArtwork.bulkCreate(artistArtwork, {transaction});
            }

            if(param['commodity_sale_type'] == enums.CommoditySaleType.ONLINE){
                let stockParam = {};

                stockParam['commodity_id'] = param['id'];
                stockParam['commodity_type'] = enums.CommodityType.ARTWORK;
                stockParam['spec_name'] = 'default';
                stockParam['fee'] = param['commodity_sale_fee'];
                stockParam['fee_type'] = enums.CurrencyType.CNY;
                stockParam['num'] = 1;
                stockParam['disp_state'] = enums.DispState.UP;
                stockParam['modifier'] = ci.DEFAULT_MODIFIER;
                stockParam['modify_time'] = Date.now();
                stockParam['create_time'] = Date.now();
                stockParam['lstate'] = enums.DataLState.ACTIVE;

                let stock = await CommodityStock.update(stockParam, {
                    where: {
                        commodity_id: param['id'],
                        commodity_type: enums.CommodityType.ARTWORK,
                    },
                    transaction,
                });

                if(stock[0] == 0){
                    stockParam['id'] = uuidv1().replace(/-/g, '');
                    await CommodityStock.create(stockParam, {transaction});
                }
            }

            await transaction.commit();

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            if(transaction){
                await transaction.rollback();
            }

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getArtwork: async function(req, res) {
        let param = req.query;

        ci.handlePageParam(param);

        const Artwork = getModel('artwork');
        const Artist = getModel('artist');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const CommodityStock = getModel('commodity_stock');

        let orderRule = [
            ['disp_state', 'ASC'],
            ['weight', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_artwork.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if(param['query_name']){
            whereRule['$t_artwork.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if(param['disp_state']){
            whereRule['$t_artwork.disp_state$'] = {
                '$eq': param['disp_state'],
            }
        }

        let artistIds = [];
        if(param['query_artist_name']){
            artistIds = await Artist.findAll({
                attributes: [
                    'id',
                ],
                where: {
                    '$t_artist.name$': {
                        '$like': '%' + param['query_artist_name'] + '%',
                    }
                },
            }).map((obj)=>{
                return obj['id'];
            });
        }

        if(param['artist_id']){
            artistIds.push(param['artist_id']);
        }

        if(artistIds.length > 0){
            whereRule['id'] = [];
            await MergeArtistArtwork.findAll({
                attributes: [
                    'artwork_id',
                ],
                where: {
                    'artist_id': artistIds,
                },
            }).map((obj)=>{
                whereRule['id'].push(obj['artwork_id']);
            });
        }

        // logger.debug(artistIds);
        // logger.debug(relArtistArtwork);

        let tsNow = moment().unix();

        let queryParam = {
            attributes: [
                'id',
                'name',
                'artist_name',
                'disp_state',
                'preview_pic_url',
                'pic_url',
                'introduce',
                'description',
                'audio_guide_id',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
                'commodity_sale_type',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let artworks = await Artwork.findAndCountAll(queryParam);
            let count = artworks.count;
            artworks = artworks.rows;

            artworks = JSON.parse(JSON.stringify(artworks));
            artworks.map((obj)=>{
                obj['pic_url'] = obj['pic_url'].split('|');
                return obj;
            });

            let artworkIds = artworks.map((obj)=>{
                return obj['id'];
            });
            let artistIds = [],
                relArtworkArtist = {};
            await MergeArtistArtwork.findAll({
                attributes: [
                    'artist_id',
                    'artwork_id',
                ],
                where: {
                    'artwork_id': artworkIds,
                },
            }).map((obj)=>{
                artistIds.push(obj['artist_id']);
                if(!relArtworkArtist[obj['artwork_id']]){
                    relArtworkArtist[obj['artwork_id']] = [];
                }
                relArtworkArtist[obj['artwork_id']].push(obj['artist_id']);
            });
            let artistMap = {};
            await Artist.findAll({
                attributes: [
                    'id',
                    'name',
                    'pic_url',
                ],
                where: {
                    'id': artistIds,
                },
            }).map((obj)=>{
                obj['pic_url'] = obj['pic_url'].split('|');
                artistMap[obj['id']] = obj;
            });

            let commodityStocks = await CommodityStock.findAll({
                attributes: [
                    'commodity_id',
                    'fee',
                ],
                where: {
                    'commodity_id': artworkIds,
                    'commodity_type': enums.CommodityType.ARTWORK,
                    'lstate': enums.DataLState.ACTIVE,
                },
            });
            let feeMap = {};
            commodityStocks.map((obj)=>{
                feeMap[obj['commodity_id']] = obj['fee'];
            });

            artworks.map((obj)=>{
                obj['artist_list'] = [];
                if(relArtworkArtist[obj['id']]){
                    relArtworkArtist[obj['id']].map((id)=> {
                        obj['artist_list'].push(artistMap[id]);
                    });
                }

                if(feeMap[obj['id']]){
                    obj['commodity_sale_fee'] = feeMap[obj['id']];
                }
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: artworks,
                size: artworks.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getArtworkDetail: async function(req, res) {
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_artwork.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_artwork.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Artwork = getModel('artwork');
        const MergeArtistArtwork = getModel('merge_artist_artwork');
        const CommodityStock = getModel('commodity_stock');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'artist_name',
                'disp_state',
                'preview_pic_url',
                'pic_url',
                'introduce',
                'description',
                'audio_guide_id',
                'modifier',
                'modify_time',
                'create_time',
                'weight',
                'commodity_sale_type',
                'spec_info',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let artwork = await Artwork.findAll(queryParam);
            artwork = JSON.parse(JSON.stringify(artwork));

            if(artwork.length < 1){
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = artwork[0];
            data['pic_url'] = data['pic_url'].split('|');

            let relsArtwork = await MergeArtistArtwork.findAll({
                attributes: [
                    'artist_id',
                ],
                where: {
                    'artwork_id': param['id'],
                },
            });
            data['artist_id'] = relsArtwork.map((obj)=>{
                return obj['artist_id'];
            });

            if(data['spec_info']){
                data['spec_info'] = JSON.parse(data['spec_info']);
            }


            if(data['commodity_sale_type'] == enums.CommoditySaleType.ONLINE){
                let commodityStock = await CommodityStock.findAll({
                    attributes: [
                        'fee',
                    ],
                    where: {
                        'commodity_id': param['id'],
                        'commodity_type': enums.CommodityType.ARTWORK,
                        'lstate': enums.DataLState.ACTIVE,
                    },
                });
                data['commodity_sale_fee'] = commodityStock[0]['fee'];
            }

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

};

function getModel(model){
    return globalContext.database['mysql']['db_muller'].import(APP_BASE_PATH + 'include/models/' + model);
}
