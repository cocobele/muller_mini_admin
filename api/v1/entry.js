const APP_BASE_PATH = '../../';
const express = require('express');
const router = express.Router();

// 路由(不鉴权或部分鉴权接口)
router.use('/auth', require('./routes/auth'));

// 路由(鉴权接口)
router.use(require(APP_BASE_PATH + 'middleware/auth').sessionValidate);
router.use(require(APP_BASE_PATH + 'middleware/auth').accessControl);

router.use('/article', require('./routes/article'));
router.use('/artwork', require('./routes/artwork'));
router.use('/artist', require('./routes/artist'));
router.use('/brand', require('./routes/brand'));
router.use('/product_cate', require('./routes/product_cate'));
router.use('/product', require('./routes/product'));
router.use('/exhibition', require('./routes/exhibition'));
router.use('/user', require('./routes/user'));
router.use('/common', require('./routes/common'));
router.use('/order', require('./routes/order'));
router.use('/mini_user', require('./routes/mini_user'));

module.exports = router;