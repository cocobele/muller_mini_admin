CREATE TABLE `t_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mime` varchar(16) NOT NULL DEFAULT '' COMMENT 'MIME',
  `height` int(11) NOT NULL DEFAULT '0' COMMENT '高度',
  `width` int(11) NOT NULL DEFAULT '0' COMMENT '宽度',
  `file_size` int(11) NOT NULL DEFAULT '-1' COMMENT '文件大小（字节）',
  `raw_url` varchar(256) NOT NULL DEFAULT '' COMMENT '原始地址',
  `file_url` varchar(256) NOT NULL DEFAULT '' COMMENT '下载地址',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(16) NOT NULL DEFAULT '' COMMENT '修改人',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '修改时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=30000000 DEFAULT CHARSET=utf8 COMMENT='图片表'
