CREATE TABLE `t_order_stock` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(64) NOT NULL COMMENT '商品skuID',
  `order_id` varchar(64) NOT NULL COMMENT '订单ID',
    `name` varchar(128) NOT NULL DEFAULT '' COMMENT '产品名称',
   `spec_name` varchar(256) NOT NULL COMMENT '规格值',
  `preview_pic_url` varchar(128) NOT NULL DEFAULT '' COMMENT '规格图片URL',
  `fee` int(11) NOT NULL COMMENT '商品单价，分',
  `fee_type` varchar(16) NOT NULL COMMENT '订单币种，CNY',
  `num` int(11) NOT NULL COMMENT '商品数量',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_stock_id` (`stock_id`),
  KEY `idx_order_id` (`order_id`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品订单表'
