CREATE TABLE `t_exhibition` (
  `id` varchar(64) NOT NULL DEFAULT '' COMMENT 'ID',
  `name` varchar(256) NOT NULL DEFAULT '' COMMENT '展览名称',
  `disp_state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '展示状态，1上架;2下架',
  `preview_pic_url` varchar(128) NOT NULL DEFAULT '' COMMENT '封面图片URL',
  `pic_url` varchar(2048) NOT NULL DEFAULT '' COMMENT '展览图片URL,用|分割',
  `introduce` text COMMENT '展览简介',
  `open_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '展出起始时间',
  `close_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '展出结束时间',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT '权重',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(16) NOT NULL DEFAULT '' COMMENT '修改人',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '修改时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='展览表'
