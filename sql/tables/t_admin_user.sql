CREATE TABLE `t_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '名称',
  `portrait_url` varchar(256) NOT NULL DEFAULT '' COMMENT '头像URL',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别|1男;2女',
  `wechat_id` varchar(64) NOT NULL DEFAULT '' COMMENT '微信号',
  `phone` varchar(64) NOT NULL DEFAULT '' COMMENT '手机号',
  `password` varchar(256) NOT NULL DEFAULT '' COMMENT '登录密码',
  `enabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '启用状态|0禁用;1启用',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(16) NOT NULL DEFAULT '' COMMENT '修改人',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '修改时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_name` (`name`),
  KEY `idx_phone` (`phone`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=10000000 DEFAULT CHARSET=utf8 COMMENT='管理后台用户';