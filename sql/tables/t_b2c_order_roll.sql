CREATE TABLE `t_b2c_order_roll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `b2c_order_id` varchar(64) NOT NULL COMMENT '交易单号',
  `old_trade_state` int(11) NOT NULL COMMENT '旧交易状态',
  `trade_state` int(11) NOT NULL COMMENT '交易状态，0未支付;1订单完成;11支付成功;12已发货;21支付失败;22订单关闭;23转入退款',
  `trade_state_desc` varchar(256) NOT NULL DEFAULT '' COMMENT '交易状态描述',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(16) NOT NULL DEFAULT '' COMMENT '修改人',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '修改时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_b2c_order_id` (`b2c_order_id`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='B2C交易单状态流转表'
