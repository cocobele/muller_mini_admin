CREATE TABLE `t_artwork` (
  `id` varchar(64) NOT NULL DEFAULT '' COMMENT 'ID',
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT '作品名称',
  `artist_name` varchar(64) NOT NULL DEFAULT '' COMMENT '艺术家名称',
  `disp_state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '展示状态，1上架;2下架',
  `preview_pic_url` varchar(128) NOT NULL DEFAULT '' COMMENT '封面图片URL',
  `pic_url` varchar(2048) NOT NULL DEFAULT '' COMMENT '作品图片URL,用|分割',
  `introduce` text COMMENT '作品简介',
  `description` text COMMENT '作品说明',
  `audio_guide_id` varchar(512) NOT NULL DEFAULT '' COMMENT '语音导览ID,用|分割',
  `spec_info` varchar(2048) NOT NULL DEFAULT '' COMMENT '规格信息',
  `weight` int(11) NOT NULL DEFAULT '0' COMMENT '权重',
  `commodity_sale_type` tinyint(4) NOT NULL DEFAULT '3' COMMENT '销售类型，1线上;2线下;3不销售',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(16) NOT NULL DEFAULT '' COMMENT '修改人',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '修改时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='艺术作品表'
