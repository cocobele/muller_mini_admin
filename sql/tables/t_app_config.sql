CREATE TABLE `t_app_config` (
  `id` varchar(128) NOT NULL DEFAULT '' COMMENT 'ID',
  `val` text NOT NULL COMMENT '值',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(16) NOT NULL DEFAULT '' COMMENT '修改人',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '修改时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务配置表'
