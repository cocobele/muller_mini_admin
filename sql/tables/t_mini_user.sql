CREATE TABLE `t_mini_user` (
  `id` varchar(64) NOT NULL DEFAULT '' COMMENT 'ID',
  `open_id` varchar(128) NOT NULL DEFAULT '' COMMENT '微信号标志符',
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT '用户名称',
  `email` varchar(128) NOT NULL DEFAULT '' COMMENT '邮件地址',
  `phone` varchar(128) NOT NULL DEFAULT '' COMMENT '联系电话',
  `address` varchar(256) NOT NULL DEFAULT '' COMMENT '地址',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(16) NOT NULL DEFAULT '' COMMENT '修改人',
  `last_login_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '最后登陆时间',
  `login_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '登陆时间',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '修改时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_open_id` (`open_id`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=10000000 DEFAULT CHARSET=utf8 COMMENT='小程序用户表';