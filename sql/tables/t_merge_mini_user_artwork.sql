  CREATE TABLE `t_merge_mini_user_artwork` (
  `mini_user_id` varchar(64) NOT NULL COMMENT '用户ID',
  `artwork_id` varchar(64) NOT NULL COMMENT '艺术作品ID',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `lstate` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态|0删除;1有效',
  `modifier` varchar(128) NOT NULL DEFAULT '' COMMENT '最后编辑者',
  `modify_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '最后编辑时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`mini_user_id`, `artwork_id`),
  KEY `idx_artwork_id_mini_user_id` (`artwork_id`, `mini_user_id`),
  KEY `idx_modify_time` (`modify_time`),
  KEY `idx_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与艺术家作品关联表'