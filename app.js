const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const compress = require('compression');
const morgan = require('morgan');
const morganJson = require('morgan-json');
const fs = require('fs');
const later = require('later');
const logger = require('./utils/logger');
const enums = require('./include/enum');
const Sequelize = require('sequelize');
const co = require("./utils/common")
const task = require('./task/dummyTask')

let globalContext = require('./include/global');
let app = express();
let sessionMid = null;

//加载配置并初始化模块
initialize();

//加载配置,配置模块
module.exports = app;

async function initialize(){
    await loadConfig();
    await initModules();
    await initDatabases();
    await initMiddleware();
    await initTasksSchedule();
}

async function initMiddleware(){
    app.use(helmet());
    app.use(compress());

    // log
    let morganFmt = morganJson({
        'http.method': ':method',
        'http.path': ':url',
        'http.status': ':status',
        'http.referrer': ':referrer',
        'http.user-agent': ':user-agent',
        'http.res-size': ':res[content-length]',
        'http.rsp-time': ':response-time',
        'http.remote-ip': ':req[X-Real-IP]'
    });
    app.use(morgan(morganFmt));

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ limit:'1mb', extended: false }));
    app.use(cookieParser());

    // 会话
    app.use(sessionMid);

    //IP处理
    // app.use(require('./middleware/misc').remoteIp);

    // API限速器
    // app.use(require('./middleware/misc').rateLimiterGlobal);

    //API接口
    app.use('/api/v1', require('./api/v1/entry'));

    app.use(function(req, res, next) {
        let err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    app.use(function(err, req, res, next) {
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};
        res.status(err.status || 500);
        res.send(err.message);
    });
}

async function initTasksSchedule(){
}

async function loadConfig() {
    //初始化配置文件
    try {
        globalContext.config = JSON.parse(fs.readFileSync('test/pro/config.json', 'utf-8'));
    } catch (e) {
        logger.error('failed to load app config file');
        logger.printExecption(e);
        process.exit(-1);
    }

    logger.debug('load app config: ' + JSON.stringify(globalContext.config));
}

async function initDatabases() {
    //初始化DB
    globalContext.database['mysql'] = {}
    globalContext.database['mysql']['db_muller'] = new Sequelize({
        host: globalContext.config['dbs']['mysql']['db_muller']['host'],
        port: globalContext.config['dbs']['mysql']['db_muller']['port'],
        username: globalContext.config['dbs']['mysql']['db_muller']['username'],
        password: globalContext.config['dbs']['mysql']['db_muller']['password'],
        database: globalContext.config['dbs']['mysql']['db_muller']['database'],
        dialect: 'mysql'
    });
}

async function initModules() {
    //初始化session模块
    const sessionModule = require('express-session');
    const sessionStoreModule = require('connect-redis')(sessionModule);
    sessionMid = sessionModule({
        secret: 'yanshanadmin:2019:tk',
        name: 'yanshanadmintk',
        saveUninitialized: true,
        resave: false,
        cookie: {
            secure: false,
            // domain: appCfg['website']['domain'],
        },
        store: new sessionStoreModule({
            url: 'redis://' + globalContext.config['dbs']['redis']['session']['host'],
            ttl: 3600*24*7,
        }),
    })
}
