
let errcodeMap = {

    // 基础返回码
    SUCCESS: [0, '成功'],
    FAILED: [10001, '未知错误'],
    TOO_OFTEN: [10002, '请求频繁'],
    PARAM_ERROR: [11001, '参数错误'],

    // 资源相关
    RES_NOT_EXISTS: [20001, '请求资源不存在'],
    CANNOT_FIND_UPLOADED_FILE: [20002, '缺少待上传文件'],

    // 账号权限相关
    LOGIN_CODE_EXPIRED: [30001, '登录验证码过期'],
    LOGIN_CODE_INVALID: [30002, '登录验证码无效'],
    NOT_LOGGED_IN: [30003, '用户未登录'],
    UNAUTHORIZED_OPT: [30004, '无操作权限'],
    USER_NOT_EXIST: [30005, '用户不存在'],
    INVALID_PHONE_NUMBER: [30006, '无效的手机号'],
    LOGIN_IP_CHANGED: [30007, '登录IP改变，请重新登录'],
    USER_SESSION_EXPIRED: [30008, '用户登录态过期，请重新登录'],
    USER_SESSION_INVALID: [30009, '用户登录态无效，请重新尝试'],
    ACCOUNT_PWD_ERROR: [30010, '账号或密码错误'],

    // 业务相关
    DATE_TIME_PARAM_ERROR: [40001, '日期参数错误'],
    ORDER_TRADE_STATE_UPDATE_NOT_ALLOWED: [41001, '非法的订单状态流转'],

};

for(let k in errcodeMap){
    let o = errcodeMap[k];
    errcodeMap[k] = {
        code: o[0],
        message: o[1],
    }
}

module.exports = errcodeMap;
