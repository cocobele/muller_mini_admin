const logger = require('../utils/logger');
const enums = require('./../include/enum');


var MODULE = {};

MODULE.handlePageParam = function(param, defaultPageSize) {
    const MAX_PAGE_SIZE = 1000;

    param.size = parseInt(param.size);

    if(!param.size){
        if(defaultPageSize){
            param.size = defaultPageSize;
        } else {
            param.size = 50;
        }
    }

    if(param.size > MAX_PAGE_SIZE){
        param.size = MAX_PAGE_SIZE;
    }

    if(!param.page || param.page < 1){
        param.page = 1;
    }
}

// 默认修改人
MODULE.DEFAULT_MODIFIER = 'admin';

module.exports = MODULE;