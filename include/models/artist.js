
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_artist', {
        id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(128),
        },
        disp_state: {
            type: DataTypes.INTEGER,
        },
        pic_url: {
            type: DataTypes.STRING(2048),
        },
        introduce: {
            type: DataTypes.TEXT,
        },
        weight: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
