
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_merge_artist_article', {
        artist_id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        article_id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        sort: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(128),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
