
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_merge_b2c_order_commodity', {
        b2c_order_id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        commodity_id: {
            type: DataTypes.STRING(64),
        },
        commodity_type: {
            type: DataTypes.INTEGER,
        },
        commodity_stock_id: {
            type: DataTypes.STRING(64),
        },
        spec_name: {
            type: DataTypes.STRING(256),
        },
        fee: {
            type: DataTypes.INTEGER,
        },
        num: {
            type: DataTypes.INTEGER,
        },
        total_fee: {
            type: DataTypes.INTEGER,
        },
        fee_type: {
            type: DataTypes.STRING(16),
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
