
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_app_config', {
        id: {
            type: DataTypes.STRING(256),
            primaryKey: true
        },
        val: {
            type: DataTypes.TEXT,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
