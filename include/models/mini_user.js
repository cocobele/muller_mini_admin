
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_mini_user', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(128),
        },
        open_id: {
            type: DataTypes.STRING(128),
        },
        gender: {
            type: DataTypes.INTEGER,
        },
        last_login_time: {
            type: DataTypes.DATE(6),
        },
        phone: {
            type: DataTypes.STRING(64),
        },
        address: {
            type: DataTypes.STRING(256),
        },
        email: {
            type: DataTypes.STRING(256),
        },
        logo: {
            type: DataTypes.STRING(256),
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
