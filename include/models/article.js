
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_article', {
        id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING(256),
        },
        disp_state: {
            type: DataTypes.INTEGER,
        },
        preview_pic_url: {
            type: DataTypes.STRING(128),
        },
        abstract: {
            type: DataTypes.TEXT,
        },
        content: {
            type: DataTypes.TEXT,
        },
        weight: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
