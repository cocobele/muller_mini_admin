
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_commodity_stock', {
        id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        commodity_id: {
            type: DataTypes.STRING(64),
        },
        commodity_type: {
            type: DataTypes.INTEGER,
        },
        spec_name: {
            type: DataTypes.STRING(256),
        },
        fee: {
            type: DataTypes.INTEGER,
        },
        fee_type: {
            type: DataTypes.STRING(16),
        },
        num: {
            type: DataTypes.INTEGER,
        },
        preview_pic_url: {
            type: DataTypes.STRING(512),
        },
        disp_state: {
            type: DataTypes.INTEGER,
        },
        weight: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
