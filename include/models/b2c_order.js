
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_b2c_order', {
        id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        commodity_name: {
            type: DataTypes.STRING(256),
        },
        total_fee: {
            type: DataTypes.INTEGER,
        },
        fee_type: {
            type: DataTypes.STRING(16),
        },
        num: {
            type: DataTypes.INTEGER,
        },
        cash_fee: {
            type: DataTypes.INTEGER,
        },
        cash_fee_type: {
            type: DataTypes.STRING(16),
        },
        client_ip: {
            type: DataTypes.STRING(64),
        },
        time_start: {
            type: DataTypes.DATE(6)
        },
        time_pay: {
            type: DataTypes.DATE(6)
        },
        time_end: {
            type: DataTypes.DATE(6)
        },
        trade_state: {
            type: DataTypes.INTEGER,
        },
        trade_state_desc: {
            type: DataTypes.STRING(256),
        },
        openid: {
            type: DataTypes.STRING(128),
        },
        remark: {
            type: DataTypes.STRING(512),
        },
        contact_name: {
            type: DataTypes.STRING(16),
        },
        contact_addr: {
            type: DataTypes.STRING(512),
        },
        contact_phone: {
            type: DataTypes.STRING(16),
        },
        express_name: {
            type: DataTypes.STRING(64),
        },
        express_order_id: {
            type: DataTypes.STRING(128),
        },
        pay_channel_type: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
