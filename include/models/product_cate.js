
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_product_cate', {
        id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(128),
        },
        preview_pic_url: {
            type: DataTypes.STRING(128),
        },
        disp_state: {
            type: DataTypes.INTEGER,
        },
        weight: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
