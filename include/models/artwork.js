
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_artwork', {
        id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(128),
        },
        artist_name: {
            type: DataTypes.STRING(64),
        },
        disp_state: {
            type: DataTypes.INTEGER,
        },
        preview_pic_url: {
            type: DataTypes.STRING(128),
        },
        pic_url: {
            type: DataTypes.STRING(2048),
        },
        introduce: {
            type: DataTypes.TEXT,
        },
        description: {
            type: DataTypes.TEXT,
        },
        audio_guide_id: {
            type: DataTypes.STRING(512),
        },
        spec_info: {
            type: DataTypes.STRING(2048),
        },
        commodity_sale_type: {
            type: DataTypes.INTEGER,
        },
        weight: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
