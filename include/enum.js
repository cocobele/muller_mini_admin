var _enumValidator = {}

var ENUM = {

    // 数据状态
    DataLState: {
        ACTIVE: 1,
        DELETED: 0,
        INACTIVE:2,
    },

    // 上下架状态
    DispState: {
        UP: 1,
        DOWN: 2,
    },

    // 商品销售类型
    CommoditySaleType: {
        ONLINE: 1,
        OFFLINE: 2,
        NOT_FOR_SALE: 3,
    },

    // 商品类型
    CommodityType: {
        ARTWORK: 1,
        PRODUCT: 2,
    },

    // 币种类型
    CurrencyType: {
        CNY: 'CNY',
    },

    // 订单状态
    B2cOrderState: {
        NOT_PAY: 0,
        SUCCESS: 1,
        USER_PAID: 11,
        SHIPPED: 12,
        REFUNDING: 13,
        PAY_FAILED: 21,
        CLOSED: 22,
        REFUNDED: 23,
    },

};

ENUM.validateEnum = function(enumName, enumVal){
    if(typeof ENUM[enumName] == 'undefined'){
        throw 'Enum ' + enumName + ' not exists';
    }

    if(!_enumValidator[enumName]){
        _enumValidator[enumName] = {};
        for(var k in ENUM[enumName]){
            _enumValidator[enumName][ENUM[enumName][k]] = true;
        }
    }

    return _enumValidator[enumName][enumVal] === true;
}

module.exports = ENUM;