
class Logger {

    constructor(){
    }

    debug(msg){
        this.stdout({
            'logger.debug': msg,
        })
    }

    info(msg){
        this.stdout({
            'logger.info': msg,
        })
    }

    error(msg){
        this.stdout({
            'logger.error': msg,
        })
    }

    stdout(msgObj){
        console.log(JSON.stringify(msgObj));
    }

}
var logger = new Logger();


module.exports = {

    error: function(msg){
        logger.error(msg);
    },

    debug: function(msg){
        logger.debug(msg);
    },

    info: function(msg){
        logger.info(msg);
    },

    printExecption: function(e){
        logger.error(e.toString());
        if(e.stack){
            logger.error(e.stack);
        }
    },
    
};
