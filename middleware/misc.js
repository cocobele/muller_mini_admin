const logger = require('../utils/logger');
const _ = require('lodash');
const enums = require('./../include/enum');
const cu = require('../utils/common');
const RateLimiter = require('rolling-rate-limiter');

var rateLimiterNSGlobal = null;

module.exports = {

    remoteIp: async function(req, res, next) {
        req.remoteIp = req.get('X-Real-IP');

        if(req.remoteIp && req.session){
            req.session.remoteIp = req.remoteIp;
        }
        
        next();
    },

    rateLimiterGlobal: async function(req, res, next) {
        if(!rateLimiterNSGlobal){
            rateLimiterNSGlobal = createRateLimiter('ns_global');
        }
        
        if(!req.session || !req.session.remoteIp){
            return next();
        }

        try {
            await checkRateLimit(rateLimiterNSGlobal, req.session.remoteIp);
        } catch (e) {
            logger.error('rate limit triggered(' + req.session.remoteIp + ')')
            return res.status(429).json({
                ret_code: enums.ReturnCode.TOO_OFTEN,
            });
        }

        next();
    },

};

function checkRateLimit(limiter, identify) {
    return new Promise(function(resolve, reject){
        limiter(identify, function(err, timeLeft) {
            if (err) {
                reject();
            } else if (timeLeft) {
                reject();
            } else {
                resolve();
            }
        });
    })
}

function createRateLimiter(namespace){
    var globalContext = require('../include/global');
    var redisUrl = 'redis://' + globalContext.config['dbs']['redis']['rate_limiter']['host'];
    var redisClient = require('redis').createClient(redisUrl);

    return RateLimiter({
        redis: redisClient,
        namespace: namespace,
        interval: 3*60*1000,
        maxInInterval: 120,
    });
}