const logger = require('../utils/logger');
const _ = require('lodash');
const enums = require('./../include/enum');
const cu = require('../utils/common');
const ec = require('../include/errcode');
const request = require('request-promise-native');
const lru = require('lru-cache');

module.exports = {

    sessionValidate: async function(req, res, next) {
        let tNowSec = Math.ceil(new Date().getTime()/1000),
            session = req.session;

        //检查登录
        if(!session || !session.userId){
            return res.status(401).json({
                ret_code: ec.NOT_LOGGED_IN.code,
                ret_msg: ec.NOT_LOGGED_IN.message,
            });
        }

        // 是否会话过期
        if(tNowSec > session.expireIn){
            return res.status(401).json({
                ret_code: ec.USER_SESSION_EXPIRED.code,
                ret_msg: ec.USER_SESSION_EXPIRED.message,
            });
        }

        next();

    },

    accessControl: async function(req, res, next) {
        // pass
        next();
    },

};
